## NitsNets - Prueba Backend Daniel Mora Pastor
## Resumen

Proyecto **Laravel** correspondiente a la prueba backend del proceso de selección para NitsNets.

## Set up Servidor de Desarrollo

* Requisitos mínimos

	* PHP >= 5.4
	* MCrypt PHP Extension
	* MySQL >= 5.5


* Descargar Composer

```shell
[nitsnets]$ curl -sS https://getcomposer.org/installer | php
[nitsnets]$ mv composer.phar /usr/local/bin/composer
```

Para más información : <http://getcomposer.org/download/>


* Ejecutar Composer

```shell
[nitsnets]$ composer install
```

**Composer debe instalarse y ejecutarse SIEMPRE para la instalación del proyecto, independientemente del entorno en el que se vaya a instalar (Vagrant, AMP, etc.).**

* Descargar VirtualBox: <https://www.virtualbox.org/wiki/Downloads>

* Descargar Vagrant: <http://downloads.vagrantup.com/>

* Instalar Vagrant Host Manager

```shell
[nitsnets]$ vagrant plugin install vagrant-hostmanager
```

<https://github.com/smdahlen/vagrant-hostmanager>


* Iniciar Vagrant

```shell
[nitsnets]$ vagrant up
```

Si se desea ejecutar la aplicación en un entorno AMP, mover el proyecto a la carpeta htdocs y acceder via localhost,
en función de cómo se haya configurado el servidor de desarrollo. 

## Configuración de la base de datos

Si se ha configurado el entorno de desarrollo via Vagrant, basta con acceder con un cliente MySQL a la URL *dev.nts.io*
con el usuario *root* y dejando la contraseña vacía. Habrá creada una base de datos llamada **nts**.

Si se ha desplegado el proyecto en un entorno AMP, habrá que acceder al servidor de base de datos de desarrollo
y crear la base de datos **nts** SIN TABLAS.

Una vez se tiene creada la base de datos, se pueden ejecutar los scripts de base de datos que se encuentran dentro de a carpeta
**/app/database/scripts**:
	* En primer lugar ejecutar el script *nts_structure.sql*.
	* Posteriormente, ejecutar el script *nts_data.sql*.

Una vez se ha configurado la base de datos, para que el proyecto funcione, es necesario establecer la configuración de conexión con la base de datos. Para ello, en el directorio raíz del proyecto, es necesario crear un fichero llamado **.env.local.php** con el siguiente contenido:

```
return array(
    'DEBUG' => 1,

    'MYSQL_HOST' => '<hostname>',
    'MYSQL_DB' => '<database>',
    'MYSQL_USER' => '<user>',
    'MYSQL_PASSWORD' => '<password>',
);
```

Este fichero no se incluye en el control de versiones por seguridad.

## Acceso a la aplicación

Una vez se han realizado todos estos pasos, ya se tiene el proyecto desplegado y se puede acceder a él. Si se está usando un
entorno Vagrant, puede accederse a través de la URL **dev.nts.io**.

El proyecto puede verse en funcionamiento [aquí](http://ec2-54-77-3-7.eu-west-1.compute.amazonaws.com/).


## Esquema entidad relación

El esquema Entidad-Relación (de ahora en adelante EER) realizado para esta prueba es el que se ve en esta imagen:

![EER](https://bytebucket.org/moraDmp30/prueba-nitsnets/raw/6bea900775ff61b2feb7699ab6fcc01e61517b90/app/database/scripts/eer.png)

Las tablas que encontramos en este EER son las siguientes:

* *Languages*: incluye una relación de todos los idiomas disponibles en la aplicación. Tiene los siguientes campos:
	* *Id*: identificador único del idioma.
	* *Name*: nombre del idioma.
	* *Prefix*: prefijo internacional del idioma.
	* *Created_at*, *updated_at*, *deleted_at*: campos necesarios para emplear el ORM de Laravel.
* *Categories*: contiene todas las categorías existentes en la aplicación. Tiene los siguientes campos:
	* *Id*: identificador único de la categoría.
	* *Created_at*, *updated_at*, *deleted_at*: campos necesarios para emplear el ORM de Laravel.
* *Category_language*: tabla resultado de la relación N:N de las categorías con los idiomas. Incluye los siguientes campos:
	* *Category_id*: identificador de la categoría. Clave ajena hacia la tabla *Categories*.
	* *Language_id*: identificador del idioma. Clave ajena hacia la tabla *Languages*.
	* *Name*: nombre de la categoría en un idioma determinado. Al incluir este campo en la tabla pivote de la relación, se da soporte a la posibilidad de emplear múltiples idiomas sin necesidad de realizar cambios estructurales en la base de datos.
	* *Created_at*, *updated_at*, *deleted_at*: campos necesarios para emplear el ORM de Laravel.
* *Colours*: contiene todos los colores existentes en la aplicación. Tiene los siguientes campos:
	* *Id*: identificador único del color.
	* *Created_at*, *updated_at*, *deleted_at*: campos necesarios para emplear el ORM de Laravel.
* *Colour_language*: tabla resultado de la relación N:N de los colores con los idiomas. Incluye los siguientes campos:
	* *Colour_id*: identificador del color. Clave ajena hacia la tabla *Colours*.
	* *Language_id*: identificador del idioma. Clave ajena hacia la tabla *Languages*.
	* *Name*: nombre del color en un idioma determinado. Al incluir este campo en la tabla pivote de la relación, se da soporte a la posibilidad de emplear múltiples idiomas sin necesidad de realizar cambios estructurales en la base de datos.
	* *Created_at*, *updated_at*, *deleted_at*: campos necesarios para emplear el ORM de Laravel.
* *Products*: contiene todos los productos existentes en la aplicación. Tiene los siguientes campos:
	* *Id*: identificador único del producto.
	* *Price*: precio del producto.
	* *Category_id*: categoría a la que pertenece el producto. clave ajena hacia la tabla *Categories*.
	* *Created_at*, *updated_at*, *deleted_at*: campos necesarios para emplear el ORM de Laravel.
* *Colour_product*: tabla resultado de la relación N:N de los productos con los colores. Incluye los siguientes campos:
	* *Colour_id*: identificador del color. Clave ajena hacia la tabla *Colours*.
	* *Product_id*: identificador del producto. Clave ajena hacia la tabla *Products*.
	* *Created_at*, *updated_at*, *deleted_at*: campos necesarios para emplear el ORM de Laravel.
* *Language_product*: tabla resultado de la relación N:N de los productos con los idiomas. Incluye los siguientes campos:
	* *Product_id*: identificador del producto. Clave ajena hacia la tabla *Product*.
	* *Language_id*: identificador del idioma. Clave ajena hacia la tabla *Languages*.
	* *Name*: nombre del color en un idioma determinado. Al incluir este campo en la tabla pivote de la relación, se da soporte a la posibilidad de emplear múltiples idiomas sin necesidad de realizar cambios estructurales en la base de datos.
	* *Description*: descripción del producto en un idioma determinado. Ocurre lo mismo que con el campo *Name*.
	* *Created_at*, *updated_at*, *deleted_at*: campos necesarios para emplear el ORM de Laravel.


## Preguntas planteadas para la prueba

En este apartado se responden las preguntas planteadas en el documento de enunciado de la prueba.

* **Si se tuviera que añadir un nuevo idioma, ¿Qué cambios (si son necesarios) harían falta tanto a nivel de PHP como de Base de datos?**

* En caso de querer añadir un nuevo idioma, basta con añadir este idioma a la tabla *Languages* y, además, a las tablas *Language_product*, *Colour_language* y *Category_Language* hay que añadir tantos registros como filas existan en las tablas *Products*, *Colours* y *Categories* respesctivamente. Por ejemplo, si añadimos un idioma con identificador 4 y existen 12 productos, habrá que añadir 12 registros a la tabla *Language_product* donde *language_id* tenga el valor 4 y *product_id* el valor del identificador de cada producto.
* No es necesario realizar ningún cambio en el código PHP de la aplicación, ya que se ha desarrollado para que se generen dinámicamente los registros para cada idioma a la hora de generar las vistas, guardar los resultados, etc.



* **Se nos pide realizar un buscador (dentro de nuestro cms usando Mysql+PHP) de productos con las condiciones expuestas bajo. ¿Qué query (SQL) o conjunto de ellas usarías? Razona tu respuesta.**
	* **Búsqueda por identificador, precio y categoría.** 
	* **En el listado resultante debe de mostrarse toda la información del  producto (nombre, descripción, precio, color y categoría).**
	* ** Se podrá ordenar por nombre y precio.**
	* ** Existen al menos 100.000 productos, 20 categorías y 10 colores,  cada uno con los datos traducibles en los 3 idiomas indicados.**
	* ** La búsqueda se debe realizar en el menor tiempo posible,  mostrándose un máximo de 50 resultados por página. **

* Al utilizar Laravel para desarrollar esta prueba, no es necesario realizar ninguna query SQL directamente. Para implementar este buscador, bastaría con implementar el siguiente método dentro del modelo Product, que se encarga de realizar la consulta a la base de datos:

```
public static function searchProducts($searchFields, $orderFields, $page)
{
	/*
     * Query “dummy” para el caso de que no haya
     * filtros de búsqueda y ordenación
     */
	$query = Product::where(‘id’, ‘>’, 0);

   	// Se aplican los filtros de búsqueda
   	foreach ($searchFields as $field => $value) {
   		$query = $query->where(
           	$field,
            '=',
            $value
        );
   }
   // Se aplican los filtros de ordenación
   foreach ($orderFields as $order) {
   		$query = $query->orderBy(
        	$order['field'],
        	$order['direction']
      	);
   	}

   	/*
	 * Take se utiliza para limitar los resultados a 50
     * Offset se usa para saltarse los primeros 50*(page - 1)
     * resultados para aplicar paginación.
     */
   	return $query
   		->take(50)
      	->offset(50*($page - 1))
      	->get();
}

```

* Los filtros de búsqueda se aplican al recorrer el array $searchFields. Al pasar un array, se da la posibilidad de que el número de campos por los que se busca sea dinámico, por si en un futuro se desean modificar estos filtros de búsqueda.

* Con los filtros de ordenación ocurre lo mismo que con los filtros de búsqueda, recorriendo el array $orderFields, que tiene el campo por el que se ordena y el sentido de la ordenación para cada filtro.

* La variable $page sirve para paginar los resultados y mostrarlos de 50 en 50, para que se usan los métodos take (para obtener un número determinado de elementos) y offset (cuántos elementos se salta en la consulta) del modelo.

* Una vez se tiene implementado este modelo, en el controlador  ProductsController, en el método que se encargue de realizar la búsqueda, bastaría obtener vía GET los parámetros de búsqueda, ordenación y la página, formatearlos de forma que sean válidos para el modelo e invocar al método searchProducts. 

* Al haber 100.000 productos, para que la query y el procesamiento de resultados tarde lo menos posible, se obtienen los resultados de búsqueda de 50 en 50 elementos. Cuando se realiza un cambio de página para mostrar los siguientes 50 elementos, esto puede hacerse vía AJAX pasando los mismos parámetros de búsqueda y ordenación y cambiando la página. Una vez se ha realizado la nueva consulta, se regenerará la tabla de resultados sin necesidad de recargar la página y en un tiempo mínimo.

