class php {

  package {
    "php5-fpm":ensure => installed;
    "php5-common":ensure => installed;
    "php5-cli":ensure => installed;
    "php5-curl":
      ensure  => installed,
      notify  => Service["php5-fpm"];
    "php5-memcache":
      ensure  => installed,
      notify  => Service["php5-fpm"];
    "php5-sqlite":
      ensure  => installed,
      notify  => Service["php5-fpm"];
    "php-apc":
      ensure  => installed,
      notify  => Service["php5-fpm"];
    "php5-mysql":
      ensure  => installed,
      notify  => Service["php5-fpm"];
    "php5-mcrypt":
      ensure  => installed,
      notify  => Service["php5-fpm"];
  }

  file { "/var/log/php":
    ensure => "directory",
  }  

  service { "php5-fpm":
    ensure  => "running",
    require => Package["php5-fpm"],
  }
}