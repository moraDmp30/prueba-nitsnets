#####################################################################################

# Create Permissions

GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '';
FLUSH PRIVILEGES;



#####################################################################################

# Create Database

DROP DATABASE IF EXISTS nts;
CREATE DATABASE nts;
USE nts;


#####################################################################################
