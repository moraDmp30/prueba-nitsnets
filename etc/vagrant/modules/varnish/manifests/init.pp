class varnish {

  package {
    "varnish":ensure => installed;
  }


  file { "/etc/default/varnish":
    ensure  => 'link',
    force   => true,
    replace => true,
    source => "/vagrant/etc/vagrant/modules/varnish/files/varnish",
    require => Package["varnish"],
    notify  => Service["varnish"],
  }

  service { "varnish":
    ensure  => "running",
    require => Package["varnish"],
  }
  
}