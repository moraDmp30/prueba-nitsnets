clear;
vendor/bin/phpcs --standard=PSR2 --ignore=/etc/ --ignore=/public/ --ignore=/vendor/ --ignore=/app/storage/ --ignore=/app/views/ --ignore=/app/database/migrations/ app/;
