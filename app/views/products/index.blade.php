@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-lg-12 page-header">
            <h1 class="pull-left">Productos</h1>
            <a class="pull-right btn btn-primary" href="{{ URL::route('products.create') }}">
                <i class="fa fa-plus"></i> A&ntilde;adir Productos
            </a>
        </div>
    </div>
    <div class="row" id="products-row">
        @include('products.table')
    </div>
@stop
