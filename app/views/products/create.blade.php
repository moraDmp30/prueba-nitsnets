@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-12 page-header">
        <h1 class="pull-left">
            A&ntilde;adir Producto
        </h1>
    </div>
</div>
<!-- /.row -->
@include('products.form', array('languages_form' => $languages, 'edit' => false, 'product_id' => null, 'category_id' => null, 'product_colours' => array()))

@stop

@section('bottomScripts')
    @parent
    <script type="text/javascript" src="/js/products.js"></script>
@stop
