<table class="table table-condensed table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            @foreach ($languages as $language)
                <th>Nombre {{ $language->name }}</th>
            @endforeach
            <th>Precio</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                @foreach ($product->languages as $language)
                    <td>{{ $language->pivot->name }}</td>
                @endforeach
                <td>{{ $product->price }} &euro;</td>
                <td><a href="{{ URL::route('products.show', array($product->id)) }}"><i class="fa fa-lg fa-eye"></i></a></td>
            </tr>
        @endforeach
    </tbody>
</table>
