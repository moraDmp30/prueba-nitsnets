@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 page-header">
        <h1 class="pull-left">
            {{ $product->languages[0]->pivot->name }}
        </h1>
        <div class="btn-toolbar pull-right">
            {{ Form::open(array('method' => 'delete')) }}
                <a class="btn btn-primary" href="{{ URL::route('products.edit', array($product->id)) }}">
                    <i class="fa fa-pencil"></i> Editar Producto
                </a>
                <button type="button" class="btn btn-danger" id="delete-product-button"><i class="fa fa-trash-o"></i> Borrar</button>
            {{ Form::close() }}

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">

        <div class="form-group">
            <label>Identificador:</label>
            <p class="form-control-static">
                #{{ $product->id }}
            </p>
        </div>

        <div class="form-group">
            <label>Precio:</label>
            <p class="form-control-static">
                 {{ number_format($product->price, 2) }} &euro;
            </p>
        </div>

        <div class="form-group">
            <label>Categor&iacute;a:</label>
            <p class="form-control-static">
                 {{ $product->getCategoryName() }}
            </p>
        </div>

    <div class="form-group">
        <label>Colores:</label>
        <p class="form-control-static">
             {{ $product->getColours() }}
        </p>
    </div>

    </div>

    <div class="col-lg-6">

        @foreach ($product->languages as $language)
            <div class="form-group">
                <label>Nombre ({{$language->name }}):</label>
                <p class="form-control-static">
                     {{ $language->pivot->name }}
                </p>
            </div>

            <div class="form-group">
                <label>Descripci&oacute;n ({{$language->name }}):</label>
                <p class="form-control-static">
                     {{ $language->pivot->description }}
                </p>
            </div>
        @endforeach

    </div>
    
</div>

@stop

@section('bottomScripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#delete-product-button').click(function() {
                var form = $(this).closest('form');
                if (confirm('¿Deseas borrar este producto?') == true) {
                    form.submit();
                }
            });
        });
    </script>
@stop
