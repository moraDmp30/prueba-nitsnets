<div class="row">
    @if ($edit == true)
        {{ Form::open(array('route' => array('products.update', $product_id), 'method' => 'put')) }}
    @else
        {{ Form::open(array('route' => 'products.store')) }}
    @endif

    <div class="col-lg-6">
        @foreach ($languages_form as $language)
            <div class="form-group">
                {{ Form::label('name_' . $language->prefix, 'Nombre (' . $language->name . '):', array('class' => 'control-label')) }}
                <label class="control-label error-label" style="display: none;">
                    El nombre en {{ strtolower($language->name) }} no puede estar vac&iacute;o
                </label>
                {{ Form::text('name[' . $language->id . ']', ($language->pivot ? $language->pivot->name : null), array('class' => 'form-control name-control', 'id' => 'name_' . $language->prefix)) }}
            </div>

            <div class="form-group">
                {{ Form::label('description_' . $language->prefix, 'Descripci&oacute;n (' . $language->name . '):', array('class' => 'control-label')) }}
                <label class="control-label error-label" style="display: none;">
                    La descripci&oacute;n en {{ strtolower($language->name) }} no puede estar vac&iacute;a
                </label>
                {{ Form::textarea('description[' . $language->id . ']', ($language->pivot ? $language->pivot->description : null), array('class' => 'form-control description-control', 'id' => 'description_' . $language->prefix)) }}
            </div>
        @endforeach

        <div class="form-group">
            {{ Form::button('Guardar', array('class' => 'btn btn-primary', 'id' => 'product-save-button')) }}
            @if ($edit == true)
                <a href="{{ URL::route('products.show', array($product_id)) }}" class="btn btn-danger">Cancel</a>
            @else
                <a href="{{ URL::route('products.index') }}" class="btn btn-danger">Cancel</a>
            @endif
        </div>

    </div>

    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('price', 'Precio:', array('class' => 'control-label')) }}
            <label class="control-label error-label" id="price-error-label" style="display: none;">
                El precio debe ser un n&uacute;mero positivo con 2 decimales como m&aacute;ximo (p.e. 12.00)
            </label>
            {{ Form::text('price', (!empty($product->price) ? $product->price : null), array('class' => 'form-control')) }}

        </div>

        <div class="form-group">
            {{ Form::label('category_id', 'Categor&iacute;a:', array('class' => 'control-label')) }}
            <label class="control-label error-label" id="category-error-label" style="display: none;">
                Debes elegir una categor&iacute;a
            </label>
            <select name="category_id" id="category_id" class="form-control">
                <option value="">Elige una categor&iacute;a...</option>
                @foreach ($categories as $category)
                    @if ($category->id == $category_id)
                        <option value="{{ $category->id }}" selected="selected">{{ $category->languages[0]->pivot->name }}</option>
                    @else
                        <option value="{{ $category->id }}">{{ $category->languages[0]->pivot->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>Colores:</label>
            @foreach ($colours as $colour)
                <div class="checkbox">
                    <label>
                        @if (in_array($colour->id, $product_colours))
                            <input type="checkbox" value="{{ $colour->id }}" name="colour[]" checked="checked"> {{ $colour->languages[0]->pivot->name }}
                        @else
                            <input type="checkbox" value="{{ $colour->id }}" name="colour[]"> {{ $colour->languages[0]->pivot->name }}
                        @endif
                    </label>
                </div>
            @endforeach
        </div>
    </div>

    {{ Form::close() }}

</div>
