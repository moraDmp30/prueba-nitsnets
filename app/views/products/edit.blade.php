@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-12 page-header">
        <h1 class="pull-left">
            Editar Producto
        </h1>
    </div>
</div>
<!-- /.row -->
@include('products.form', array('languages_form' => $product->languages, 'edit' => true, 'product_id' => $product->id, 'category_id' => $product->category_id, 'product_colours' => $product->getColoursId()))

@stop

@section('bottomScripts')
    @parent
    <script type="text/javascript" src="/js/products.js"></script>
@stop
