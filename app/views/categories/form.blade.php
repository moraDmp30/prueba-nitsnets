<div class="row">
    <div class="col-lg-6">
        @if ($edit == true)
            {{ Form::open(array('route' => array('categories.update', $category_id), 'method' => 'put')) }}
        @else
            {{ Form::open(array('route' => 'categories.store')) }}
        @endif

        @foreach ($languages_form as $language)
            <div class="form-group">
                {{ Form::label('name_' . $language->prefix, 'Nombre (' . $language->name . '):', array('class' => 'control-label')) }}
                <label class="control-label error-label" style="display: none;">
                    El nombre en {{ strtolower($language->name) }} no puede estar vac&iacute;o
                </label>
                {{ Form::text('name[' . $language->id . ']', ($language->pivot ? $language->pivot->name : null), array('class' => 'form-control name-control', 'id' => 'name_' . $language->prefix)) }}
            </div>
        @endforeach

        <div class="form-group">
            {{ Form::button('Guardar', array('class' => 'btn btn-primary', 'id' => 'category-save-button')) }}
            @if ($edit == true)
                <a href="{{ URL::route('categories.show', array($category_id)) }}" class="btn btn-danger">Cancel</a>
            @else
                <a href="{{ URL::route('categories.index') }}" class="btn btn-danger">Cancel</a>
            @endif
        </div>
        {{ Form::close() }}
    </div>
</div>
