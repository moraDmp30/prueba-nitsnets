@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-12 page-header">
        <h1 class="pull-left">
            Editar Categor&iacute;a
        </h1>
    </div>
</div>
<!-- /.row -->
@include('categories.form', array('languages_form' => $category->languages, 'edit' => true, 'category_id' => $category->id))

@stop

@section('bottomScripts')
    @parent
    <script type="text/javascript" src="/js/categories.js"></script>
@stop
