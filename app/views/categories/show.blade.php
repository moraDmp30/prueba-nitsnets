@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 page-header">
        <h1 class="pull-left">
            {{ $category->languages[0]->pivot->name }}
        </h1>
        <div class="btn-toolbar pull-right">
            {{ Form::open(array('method' => 'delete')) }}
                <a class="btn btn-primary" href="{{ URL::route('categories.edit', array($category->id)) }}">
                    <i class="fa fa-pencil"></i> Editar Categor&iacute;a
                </a>
                <button type="button" class="btn btn-danger" id="delete-category-button"><i class="fa fa-trash-o"></i> Borrar</button>
            {{ Form::close() }}

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">

        <div class="form-group">
            <label>Identificador:</label>
            <p class="form-control-static">
                #{{ $category->id }}
            </p>
        </div>
        
        @foreach ($category->languages as $language)
            <div class="form-group">
                <label>Nombre ({{$language->name }}):</label>
                <p class="form-control-static">
                     {{ $language->pivot->name }}
                </p>
            </div>
        @endforeach

    </div>
</div>

@stop

@section('bottomScripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#delete-category-button').click(function() {
                var form = $(this).closest('form');
                if (confirm('¿Deseas borrar esta categoría? Se borrarán todos los productos asociados a ella!') == true) {
                    form.submit();
                }
            });
        });
    </script>
@stop
