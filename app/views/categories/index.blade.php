@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-lg-12 page-header">
            <h1 class="pull-left">Categor&iacute;as</h1>
            <a class="pull-right btn btn-primary" href="{{ URL::route('categories.create') }}">
                <i class="fa fa-plus"></i> A&ntilde;adir Categor&iacute;a
            </a>
        </div>
    </div>
    <div class="row" id="categories-row">
        @include('categories.table')
    </div>
@stop
