@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-12 page-header">
        <h1 class="pull-left">
            A&ntilde;adir Color
        </h1>
    </div>
</div>
<!-- /.row -->
@include('colours.form', array('languages_form' => $languages, 'edit' => false))

@stop

@section('bottomScripts')
    @parent
    <script type="text/javascript" src="/js/colours.js"></script>
@stop
