<table class="table table-condensed table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            @foreach ($languages as $language)
                <th>Nombre {{ $language->name }}</th>
            @endforeach
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($colours as $colour)
            <tr>
                <td>{{ $colour->id }}</td>
                @foreach ($colour->languages as $language)
                    <td>{{ $language->pivot->name }}</td>
                @endforeach
                <td><a href="{{ URL::route('colours.show', array($colour->id)) }}"><i class="fa fa-lg fa-eye"></i></a></td>
            </tr>
        @endforeach
    </tbody>
</table>
