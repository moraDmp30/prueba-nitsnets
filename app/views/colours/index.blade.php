@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-lg-12 page-header">
            <h1 class="pull-left">Colores</h1>
            <a class="pull-right btn btn-primary" href="{{ URL::route('colours.create') }}">
                <i class="fa fa-plus"></i> A&ntilde;adir Color
            </a>
        </div>
    </div>
    <div class="row" id="colours-row">
        @include('colours.table')
    </div>
@stop
