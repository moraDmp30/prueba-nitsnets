@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 page-header">
        <h1 class="pull-left">
            Color {{ $colour->languages[0]->pivot->name }}
        </h1>
        <div class="btn-toolbar pull-right">
            {{ Form::open(array('method' => 'delete')) }}
                <a class="btn btn-primary" href="{{ URL::route('colours.edit', array($colour->id)) }}">
                    <i class="fa fa-pencil"></i> Editar Color
                </a>
                <button type="button" class="btn btn-danger" id="delete-colour-button"><i class="fa fa-trash-o"></i> Borrar</button>
            {{ Form::close() }}

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label>Identificador:</label>
            <p class="form-control-static">
                #{{ $colour->id }}
            </p>
        </div>

        @foreach ($colour->languages as $language)
            <div class="form-group">
                <label>Nombre ({{$language->name }}):</label>
                <p class="form-control-static">
                     {{ $language->pivot->name }}
                </p>
            </div>
        @endforeach

    </div>
</div>

@stop

@section('bottomScripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#delete-colour-button').click(function() {
                var form = $(this).closest('form');
                if (confirm('¿Deseas borrar este color?') == true) {
                    form.submit();
                }
            });
        });
    </script>
@stop
