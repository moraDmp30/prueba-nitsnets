<div class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="{{ Request::is('products*') ? 'active' : '' }}">
                <a href="{{ URL::route('products.index') }}"><i class="fa fa-folder-open fa-fw"></i> PRODUCTOS</a>
            </li>
            <li  class="{{ Request::is('categories*') ? 'active' : '' }}">
                <a href="{{ URL::route('categories.index') }}"><i class="fa fa-tags fa-fw"></i> CATEGOR&Iacute;AS</a>
            </li>
            <li  class="{{ Request::is('colours*') ? 'active' : '' }}">
                <a href="{{ URL::route('colours.index') }}"><i class="fa fa-tint fa-fw"></i> COLORES</a>
            </li>
            <li>
                <a href="/documentacion_pdf.pdf" target="_blank"><i class="fa fa-file fa-fw"></i> DOCUMENTACI&Oacute;N</a>
            </li>
        </ul>

        <!-- /#side-menu -->
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
