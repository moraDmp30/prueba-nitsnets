<?php

class ProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();
		return View::make('products.index', array('products' => $products));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categories = Category::all();
		$colours = Colour::all();
		return View::make('products.create', array('categories' => $categories, 'colours' => $colours));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$names = Input::get('name');
		$descriptions = Input::get('description');
		$colours = Input::get('colour');
		$price = Input::get('price');
		$category = Input::get('category_id');
		$product = new Product();

		DB::beginTransaction();
		try {
			$product->category_id = $category;
			$product->price = $price;
			if (!$product->save()) {
				throw new \ErrorException(implode(', ', $product->errors()->all()));
			}
			$product->setNamesAndDescriptions($names, $descriptions);
			$product->setColours($colours);
			DB::commit();
			return Redirect::route('products.show', array($product->id))->with('message', 'Producto actualizado!');
		} catch (\Exception $e) {
			DB::rollback();
			return Redirect::route('products.create')->withErrors($e->getMessage());
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$product = Product::findOrFail($id);
		return View::make('products.show', array('product' => $product));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$product = Product::findOrFail($id);
		$categories = Category::all();
		$colours = Colour::all();
		return View::make('products.edit', array('product' => $product, 'categories' => $categories, 'colours' => $colours));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$names = Input::get('name');
		$descriptions = Input::get('description');
		$colours = Input::get('colour');
		$price = Input::get('price');
		$category = Input::get('category_id');
		$product = Product::findOrFail($id);

		DB::beginTransaction();
		try {
			$product->category_id = $category;
			$product->price = $price;
			if (!$product->save()) {
				throw new \ErrorException(implode(', ', $product->errors()->all()));
			}
			$product->setNamesAndDescriptions($names, $descriptions);
			$product->setColours($colours);
			DB::commit();
			return Redirect::route('products.show', array($id))->with('message', 'Producto actualizado!');
		} catch (\Exception $e) {
			DB::rollback();
			return Redirect::route('products.edit', array($id))->withErrors($e->getMessage());
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$product = Product::findOrFail($id)->delete();
		return Redirect::to('products')->with('message', 'Producto borrado!');
	}
}
