<?php


class CategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = Category::all();
		return View::make('categories.index', array('categories' => $categories));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('categories.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$names = Input::get('name');
		$category = new Category();
		if ($category->save()) {
			$category->setNames($names);
            return Redirect::route('categories.index')->with('message', 'Categoría creada!');
        }
        return Redirect::route('categories.create')->withErrors($category->errors());
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = Category::findOrFail($id);
		return View::make('categories.show', array('category' => $category));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Category::findOrFail($id);
		return View::make('categories.edit', array('category' => $category));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$names = Input::get('name');
		$category = Category::findOrFail($id);
		$category->setNames($names);
		return Redirect::route('categories.show', array($id))->with('message', 'Categoría actualizada!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$category = Category::findOrFail($id)->delete();
		return Redirect::to('categories')->with('message', 'Categoría borrada!');
	}
}
