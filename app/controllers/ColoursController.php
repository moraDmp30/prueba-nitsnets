<?php

class ColoursController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$colours = Colour::all();
		return View::make('colours.index', array('colours' => $colours));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('colours.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$names = Input::get('name');
		$colour = new Colour();
		if ($colour->save()) {
			$colour->setNames($names);
			return Redirect::route('colours.index')->with('message', 'Color creado!');
		}
		return Redirect::route('colours.create')->withErrors($colour->errors());
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$colour = Colour::findOrFail($id);
		return View::make('colours.show', array('colour' => $colour));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$colour = Colour::findOrFail($id);
		return View::make('colours.edit', array('colour' => $colour));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$names = Input::get('name');
		$colour = Colour::findOrFail($id);
		$colour->setNames($names);
		return Redirect::route('colours.show', array($id))->with('message', 'Color actualizado!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$colour = Colour::findOrFail($id)->delete();
		return Redirect::to('colours')->with('message', 'Color borrado!');
	}
}
