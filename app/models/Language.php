<?php


use LaravelBook\Ardent\Ardent;
use Illuminate\Database\Eloquent\Model;

class Language extends Ardent
{
    /**
     * Database table
     *
     * @var string
     */
    protected $table = 'languages';

    /**
     * Purge redundant input data
     */
    public $autoPurgeRedundantAttributes = true;

    /**
     * Mass Assignment - Fillable
     */
    //protected $fillable = array();

    /**
     * Mass Assignment - Guarded
     */
    protected $guarded = array();

    /**
     * Factory
     */
/*    public static $factory = array(
        'title'      => 'string',
        'account_id' => 'factory|Account',
        'company_id' => 'factory|Company',
        'contact_id' => 'factory|Contact'
    );*/

    public static function boot()
    {
        parent::boot();
    }

    /**
     * Override default query
     */
    public function newQuery($excludeDeleted = true)
    {
        $query = parent::newQuery();
        return $query;
    }

    /**
     * Categories relationship
     */
    public function categories()
    {
        return $this->belongsToMany('Category')->withTimestamps()->withPivot('name')->orderBy('language_id');
    }

    /**
     * Colours relationship
     */
    public function colours()
    {
        return $this->belongsToMany('Colour')->withTimestamps()->withPivot('name')->orderBy('language_id');
    }

    /**
     * Products relationship
     */
    public function products()
    {
        return $this->belongsToMany('Product')->withTimestamps()->withPivot(array('name', 'description'))->orderBy('language_id');
    }
}
