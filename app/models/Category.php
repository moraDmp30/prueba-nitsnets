<?php

use LaravelBook\Ardent\Ardent;
use Illuminate\Database\Eloquent\Model;

class Category extends Ardent
{
    /**
     * Database table
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Purge redundant input data
     */
    public $autoPurgeRedundantAttributes = true;

    /**
     * Mass Assignment - Fillable
     */
    //protected $fillable = array();

    /**
     * Mass Assignment - Guarded
     */
    protected $guarded = array();

    /**
     * Factory
     */
/*    public static $factory = array(
        'title'      => 'string',
        'account_id' => 'factory|Account',
        'company_id' => 'factory|Company',
        'contact_id' => 'factory|Contact'
    );*/

    public static function boot()
    {
        parent::boot();
        static::deleting(
            function ($category) {
                $category->languages()->detach();
                foreach ($category->products as $product) {
                    $product->languages()->detach();
                    $product->colours()->detach();
                }
                $category->products()->delete();
                return true;
            }
        );
    }

    /**
     * Override default query
     */
    public function newQuery($excludeDeleted = true)
    {
        $query = parent::newQuery();
        return $query;
    }

    /**
     * Languages relationship
     */
    public function languages()
    {
        return $this->belongsToMany('Language')->withTimestamps()->withPivot('name')->orderBy('language_id');
    }

    /**
     * Define relationship to Product model.
     */
    public function products()
    {
        return $this->hasMany('Product');
    }

    public function setNames($names)
    {
        // Delete previous names
        $this->languages()->detach();
        foreach ($names as $key => $name) {
            $this->languages()->attach($key, array('name' => $name));
        }
    }
}
