<?php


use LaravelBook\Ardent\Ardent;
use Illuminate\Database\Eloquent\Model;

class Product extends Ardent
{
    /**
     * Database table
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Purge redundant input data
     */
    public $autoPurgeRedundantAttributes = true;

    /**
     * Mass Assignment - Fillable
     */
    //protected $fillable = array();

    /**
     * Mass Assignment - Guarded
     */
    protected $guarded = array();

    /**
     * Validation rules
     */
    public static $rules = array(
        'category_id'    => 'required|integer',
        'price'          => 'required|numeric|min:0'
    );

    public static function boot()
    {
        parent::boot();
        static::deleting(
            function ($product) {
                $product->languages()->detach();
                $product->colours()->detach();
                return true;
            }
        );
    }

    /**
     * Override default query
     */
    public function newQuery($excludeDeleted = true)
    {
        $query = parent::newQuery();
        return $query;
    }

    /**
     * Categories relationship
     */
    public function category()
    {
        return $this->belongsTo('Category');
    }

    /**
     * Colours relationship
     */
    public function colours()
    {
        return $this->belongsToMany('Colour')->withTimestamps()->orderBy('colour_id');
    }

    /**
     * Language relationship
     */
    public function languages()
    {
        return $this->belongsToMany('Language')->withTimestamps()->withPivot(array('name', 'description'))->orderBy('language_id');
    }

    public function getCategoryName()
    {
        return $this->category->languages[0]->pivot->name;
    }

    public function getColours()
    {
        $names = array();
        $this->colours->each(function ($item) use (&$names) {
            array_push($names, $item->languages[0]->pivot->name);
        });
        return implode(', ', $names);
    }

    public function getColoursId()
    {
        $ids = array();
        $this->colours->each(function ($item) use (&$ids) {
            array_push($ids, $item->id);
        });
        return $ids;
    }

    public function setNamesAndDescriptions($names, $descriptions)
    {
        $this->languages()->detach();
        foreach ($names as $key => $name) {
            $this->languages()->attach($key, array('name' => $name, 'description' => $descriptions[$key]));
        }
    }

    public function setColours($colours)
    {
        $this->colours()->detach();
        foreach ($colours as $colour) {
            $this->colours()->attach($colour);
        }
    }

    public static function searchProducts($searchFields, $orderFields, $page)
    {
        /*
         * Query “dummy” para el caso de que no haya
         * filtros de búsqueda y ordenación
         */
	    $query = Product::where(‘id’, ‘>’, 0);

        // Se aplican los filtros de búsqueda
    	foreach ($searchFields as $field => $value) {
            $query = $query->where(
                $field,
                '=',
                $value
            );
        }
        // Se aplican los filtros de ordenación
        foreach ($orderFields as $order) {
            $query = $query->orderBy(
                $order['field'],
                $order['direction']
            );
        }

        /*
         * Take se utiliza para limitar los resultados a 50
         * Offset se usa para saltarse los primeros 50*(page - 1)
         * resultados para aplicar paginación.
         */
        return $query
            ->take(50)
            ->offset(50*($page - 1))
            ->get();
    }

}
