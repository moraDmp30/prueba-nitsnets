<?php

use LaravelBook\Ardent\Ardent;
use Illuminate\Database\Eloquent\Model;

class Colour extends Ardent
{
    /**
     * Database table
     *
     * @var string
     */
    protected $table = 'colours';

    /**
     * Purge redundant input data
     */
    public $autoPurgeRedundantAttributes = true;

    /**
     * Mass Assignment - Fillable
     */
    //protected $fillable = array();

    /**
     * Mass Assignment - Guarded
     */
    protected $guarded = array();

    /**
     * Factory
     */
/*    public static $factory = array(
        'title'      => 'string',
        'account_id' => 'factory|Account',
        'company_id' => 'factory|Company',
        'contact_id' => 'factory|Contact'
    );*/

    public static function boot()
    {
        parent::boot();
        static::deleting(
            function ($colour) {
                $colour->languages()->detach();
                $colour->products()->detach();
                return true;
            }
        );
    }

    /**
     * Override default query
     */
    public function newQuery($excludeDeleted = true)
    {
        $query = parent::newQuery();
        return $query;
    }

    /**
     * Colours relationship
     */
    public function products()
    {
        return $this->belongsToMany('Product')->withTimestamps()->orderBy('colour_id');
    }

     /**
     * Languages relationship
     */
    public function languages()
    {
        return $this->belongsToMany('Language')->withTimestamps()->withPivot('name')->orderBy('language_id');
    }

    public function setNames($names)
    {
        // Delete previous names
        $this->languages()->detach();
        foreach ($names as $key => $name) {
            $this->languages()->attach($key, array('name' => $name));
        }
    }
}
