# ************************************************************
# Sequel Pro SQL dump
# Versión 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: dev.nts.io (MySQL 5.5.37-0ubuntu0.12.04.1)
# Base de datos: nts
# Tiempo de Generación: 2014-07-06 13:50:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla categories
# ------------------------------------------------------------

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,NULL,NULL,NULL),
	(2,NULL,NULL,NULL),
	(3,NULL,NULL,NULL),
	(4,NULL,NULL,NULL),
	(5,NULL,NULL,NULL),
	(6,NULL,NULL,NULL);

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla category_language
# ------------------------------------------------------------

LOCK TABLES `category_language` WRITE;
/*!40000 ALTER TABLE `category_language` DISABLE KEYS */;

INSERT INTO `category_language` (`id`, `category_id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,'Categoría 1',NULL,NULL,NULL),
	(2,2,1,'Categoría 2',NULL,NULL,NULL),
	(3,3,1,'Categoría 3',NULL,NULL,NULL),
	(4,4,1,'Categoría 4',NULL,NULL,NULL),
	(5,5,1,'Categoría 5',NULL,NULL,NULL),
	(6,6,1,'Categoría 6',NULL,NULL,NULL),
	(7,1,2,'Category 1',NULL,NULL,NULL),
	(8,2,2,'Category 2',NULL,NULL,NULL),
	(9,3,2,'Category 3',NULL,NULL,NULL),
	(10,4,2,'Category 4',NULL,NULL,NULL),
	(11,5,2,'Category 5',NULL,NULL,NULL),
	(12,6,2,'Category 6',NULL,NULL,NULL),
	(13,1,3,'Kategorie 1',NULL,NULL,NULL),
	(14,2,3,'Kategorie 2',NULL,NULL,NULL),
	(15,3,3,'Kategorie 3',NULL,NULL,NULL),
	(16,4,3,'Kategorie 4',NULL,NULL,NULL),
	(17,5,3,'Kategorie 5',NULL,NULL,NULL),
	(18,6,3,'Kategorie 6',NULL,NULL,NULL);

/*!40000 ALTER TABLE `category_language` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla colour_language
# ------------------------------------------------------------

LOCK TABLES `colour_language` WRITE;
/*!40000 ALTER TABLE `colour_language` DISABLE KEYS */;

INSERT INTO `colour_language` (`id`, `colour_id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,'Verde',NULL,NULL,NULL),
	(2,1,2,'Green',NULL,NULL,NULL),
	(3,1,3,'Grün',NULL,NULL,NULL),
	(4,2,1,'Azul',NULL,NULL,NULL),
	(5,2,2,'Blue',NULL,NULL,NULL),
	(6,2,3,'Blau',NULL,NULL,NULL),
	(7,3,1,'Marrón',NULL,NULL,NULL),
	(8,3,2,'Brown',NULL,NULL,NULL),
	(9,3,3,'Braun',NULL,NULL,NULL),
	(10,4,1,'Violeta',NULL,NULL,NULL),
	(11,4,2,'Purple',NULL,NULL,NULL),
	(12,4,3,'Violett',NULL,NULL,NULL),
	(13,5,1,'Blanco',NULL,NULL,NULL),
	(14,5,2,'White',NULL,NULL,NULL),
	(15,5,3,'Weiß',NULL,NULL,NULL),
	(16,6,1,'Rosa',NULL,NULL,NULL),
	(17,6,2,'Pink',NULL,NULL,NULL),
	(18,6,3,'Rosa',NULL,NULL,NULL),
	(19,7,1,'Negro',NULL,NULL,NULL),
	(20,7,2,'Black',NULL,NULL,NULL),
	(21,7,3,'Schwarz',NULL,NULL,NULL),
	(22,8,1,'Naranja',NULL,NULL,NULL),
	(23,8,2,'Orange',NULL,NULL,NULL),
	(24,8,3,'Orange',NULL,NULL,NULL),
	(25,9,1,'Amarillo',NULL,NULL,NULL),
	(26,9,2,'Yellow',NULL,NULL,NULL),
	(27,9,3,'Gelb',NULL,NULL,NULL),
	(28,10,1,'Rojo',NULL,NULL,NULL),
	(29,10,2,'Red',NULL,NULL,NULL),
	(30,10,3,'Rot',NULL,NULL,NULL);

/*!40000 ALTER TABLE `colour_language` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla colour_product
# ------------------------------------------------------------

LOCK TABLES `colour_product` WRITE;
/*!40000 ALTER TABLE `colour_product` DISABLE KEYS */;

INSERT INTO `colour_product` (`id`, `product_id`, `colour_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,NULL,NULL,NULL),
	(2,1,2,NULL,NULL,NULL),
	(3,2,3,NULL,NULL,NULL),
	(4,2,4,NULL,NULL,NULL),
	(5,3,5,NULL,NULL,NULL),
	(6,3,6,NULL,NULL,NULL),
	(7,4,7,NULL,NULL,NULL),
	(8,4,8,NULL,NULL,NULL),
	(9,5,9,NULL,NULL,NULL),
	(10,5,10,NULL,NULL,NULL),
	(11,6,1,NULL,NULL,NULL),
	(12,6,2,NULL,NULL,NULL),
	(13,7,3,NULL,NULL,NULL),
	(14,7,4,NULL,NULL,NULL),
	(15,8,5,NULL,NULL,NULL),
	(16,8,6,NULL,NULL,NULL),
	(17,9,7,NULL,NULL,NULL),
	(18,9,8,NULL,NULL,NULL),
	(19,10,9,NULL,NULL,NULL),
	(20,10,10,NULL,NULL,NULL),
	(21,11,1,NULL,NULL,NULL),
	(22,11,2,NULL,NULL,NULL),
	(23,12,3,NULL,NULL,NULL),
	(24,12,4,NULL,NULL,NULL);

/*!40000 ALTER TABLE `colour_product` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla colours
# ------------------------------------------------------------

LOCK TABLES `colours` WRITE;
/*!40000 ALTER TABLE `colours` DISABLE KEYS */;

INSERT INTO `colours` (`id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,NULL,NULL,NULL),
	(2,NULL,NULL,NULL),
	(3,NULL,NULL,NULL),
	(4,NULL,NULL,NULL),
	(5,NULL,NULL,NULL),
	(6,NULL,NULL,NULL),
	(7,NULL,NULL,NULL),
	(8,NULL,NULL,NULL),
	(9,NULL,NULL,NULL),
	(10,NULL,NULL,NULL);

/*!40000 ALTER TABLE `colours` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla language_product
# ------------------------------------------------------------

LOCK TABLES `language_product` WRITE;
/*!40000 ALTER TABLE `language_product` DISABLE KEYS */;

INSERT INTO `language_product` (`id`, `product_id`, `language_id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,'Producto 1','Esta es la descripción del producto 1',NULL,NULL,NULL),
	(2,1,2,'Product 1','This is the description of product 1',NULL,NULL,NULL),
	(3,1,3,'Produkt 1','Das ist die Beschreibung das Produkt 1',NULL,NULL,NULL),
	(4,2,1,'Producto 2','Esta es la descripción del producto 2',NULL,NULL,NULL),
	(5,2,2,'Product 2','This is the description of product 2',NULL,NULL,NULL),
	(6,2,3,'Produkt 2','Das ist die Beschreibung das Produkt 2',NULL,NULL,NULL),
	(7,3,1,'Producto 3','Esta es la descripción del producto 3',NULL,NULL,NULL),
	(8,3,2,'Product 3','This is the description of product 3',NULL,NULL,NULL),
	(9,3,3,'Produkt 3','Das ist die Beschreibung das Produkt 3',NULL,NULL,NULL),
	(10,4,1,'Producto 4','Esta es la descripción del producto 4',NULL,NULL,NULL),
	(11,4,2,'Product 4','This is the description of product 4',NULL,NULL,NULL),
	(12,4,3,'Produkt 4','Das ist die Beschreibung das Produkt 4',NULL,NULL,NULL),
	(13,5,1,'Producto 5','Esta es la descripción del producto 5',NULL,NULL,NULL),
	(14,5,2,'Product 5','This is the description of product 5',NULL,NULL,NULL),
	(15,5,3,'Produkt 5','Das ist die Beschreibung das Produkt 5',NULL,NULL,NULL),
	(16,6,1,'Producto 6','Esta es la descripción del producto 6',NULL,NULL,NULL),
	(17,6,2,'Product 6','This is the description of product 6',NULL,NULL,NULL),
	(18,6,3,'Produkt 6','Das ist die Beschreibung das Produkt 6',NULL,NULL,NULL),
	(19,7,1,'Producto 7','Esta es la descripción del producto 7',NULL,NULL,NULL),
	(20,7,2,'Product 7','This is the description of product 7',NULL,NULL,NULL),
	(21,7,3,'Produkt 7','Das ist die Beschreibung das Produkt 7',NULL,NULL,NULL),
	(22,8,1,'Producto 8','Esta es la descripción del producto 8',NULL,NULL,NULL),
	(23,8,2,'Product 8','This is the description of product 8',NULL,NULL,NULL),
	(24,8,3,'Produkt 8','Das ist die Beschreibung das Produkt 8',NULL,NULL,NULL),
	(25,9,1,'Producto 9','Esta es la descripción del producto 9',NULL,NULL,NULL),
	(26,9,2,'Product 9','This is the description of product 9',NULL,NULL,NULL),
	(27,9,3,'Produkt 9','Das ist die Beschreibung das Produkt 9',NULL,NULL,NULL),
	(28,10,1,'Producto 10','Esta es la descripción del producto 10',NULL,NULL,NULL),
	(29,10,2,'Product 10','This is the description of product 10',NULL,NULL,NULL),
	(30,10,3,'Produkt 10','Das ist die Beschreibung das Produkt 10',NULL,NULL,NULL),
	(31,11,1,'Producto 11','Esta es la descripción del producto 11',NULL,NULL,NULL),
	(32,11,2,'Product 11','This is the description of product 11',NULL,NULL,NULL),
	(33,11,3,'Produkt 11','Das ist die Beschreibung das Produkt 11',NULL,NULL,NULL),
	(34,12,1,'Producto 12','Esta es la descripción del producto 12',NULL,NULL,NULL),
	(35,12,2,'Product 12','This is the description of product 12',NULL,NULL,NULL),
	(36,12,3,'Produkt 12','Das ist die Beschreibung das Produkt 12',NULL,NULL,NULL);

/*!40000 ALTER TABLE `language_product` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla languages
# ------------------------------------------------------------

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;

INSERT INTO `languages` (`id`, `name`, `prefix`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Español','ES',NULL,NULL,NULL),
	(2,'Inglés','EN',NULL,NULL,NULL),
	(3,'Alemán','GE',NULL,NULL,NULL);

/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla products
# ------------------------------------------------------------

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `price`, `category_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,5.00,1,NULL,NULL,NULL),
	(2,10.00,1,NULL,NULL,NULL),
	(3,20.00,2,NULL,NULL,NULL),
	(4,9.99,2,NULL,NULL,NULL),
	(5,19.95,3,NULL,NULL,NULL),
	(6,25.00,3,NULL,NULL,NULL),
	(7,15.00,4,NULL,NULL,NULL),
	(8,30.00,4,NULL,NULL,NULL),
	(9,50.00,5,NULL,NULL,NULL),
	(10,4.00,5,NULL,NULL,NULL),
	(11,10.00,6,NULL,NULL,NULL),
	(12,8.50,6,NULL,NULL,NULL);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
