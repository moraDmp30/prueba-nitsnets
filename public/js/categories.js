$(document).ready(function() {
    $('#category-save-button').click(function() {
        var result = true;
        var form = $(this).closest('form');

        $('.form-group').removeClass('has-error');
        $('.error-label').hide();

        $('.name-control').each(function(index, element) {
            var value = $(element).val();
            var group = $(element).parents('.form-group');
            if (isEmpty(value) === true) {
                group.addClass('has-error');
                group.find('.error-label').show();
                result = false;
            }
        });

        if (result === true) {
            form.submit();
        }
    });
});
