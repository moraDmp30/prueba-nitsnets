function isPrice(price)
{
    var regex = new RegExp(/^[0-9]+(\.[0-9]{1,2})?$/);
    return (!(regex.test(price) === false
        || $.isNumeric(price) == false
        || parseFloat(price) < 0
    ));
}

function isIntegerGreaterThanZero(number)
{
    var regex = new RegExp(/^[0-9]+$/);
    return (!(regex.test(number) === false
        || $.isNumeric(number) == false
        || parseInt(number) <= 0
    ));
}

function isEmpty(text)
{
    return (text == null
        || text == undefined
        || text.trim() == '');
}
