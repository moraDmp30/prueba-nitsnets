$(document).ready(function() {
    $('#product-save-button').click(function() {
        var result = true;
        var form = $(this).closest('form');
        var price, category;

        price = $('#price').val();
        category = $('#category_id').val();

        $('.form-group').removeClass('has-error');
        $('.error-label').hide();

        $('.name-control').each(function(index, element) {
            var value = $(element).val();
            var group = $(element).parents('.form-group');
            if (isEmpty(value) === true) {
                group.addClass('has-error');
                group.find('.error-label').show();
                result = false;
            }
        });

        $('.description-control').each(function(index, element) {
            var value = $(element).val();
            var group = $(element).parents('.form-group');
            if (isEmpty(value) === true) {
                group.addClass('has-error');
                group.find('.error-label').show();
                result = false;
            }
        });

        if (isPrice(price) === false) {
            $('#price').parents('.form-group').addClass('has-error');
            $('#price-error-label').show();
            result = false;
        }

        if (isIntegerGreaterThanZero(category) === false) {
            $('#category_id').parents('.form-group').addClass('has-error');
            $('#category-error-label').show();
            result = false;
        }

        if (result === true) {
            form.submit();
        }
    });
});
